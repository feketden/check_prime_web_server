#include "soapcalcService.h"
#include "calc.nsmap"
#include "apache_gsoap.h"
IMPLEMENT_GSOAP_SERVER()
extern "C" int soap_serve(struct soap *soap)
{
  calcService service(soap);
  int err = service.serve();
  service.destroy();
  return err;
}
int calcService::checkPrime(int a, double &result)
{
	result = 1;
	// Corner case
	if (a <= 1)
		return SOAP_OK;

	// Check from 2 to n-1
	for (int i = 2; i < a; i++)
	{
		if (a % i == 0)
		{
			result = 0;
			return SOAP_OK;
		}
	}

  return SOAP_OK;
} 
int calcService::add(double a, double b, double &result)
{
  result = a + b;
  return SOAP_OK;
} 
int calcService::sub(double a, double b, double &result)
{
  result = a - b;
  return SOAP_OK;
} 
int calcService::mul(double a, double b, double &result)
{
  result = a * b;
  return SOAP_OK;
} 
int calcService::div(double a, double b, double &result)
{
  result = b != 0 ? a / b : 0.0;
  return SOAP_OK;
} 
int calcService::pow(double a, double b, double &result)
{
  result = ::pow(a, b);
  return SOAP_OK;
}

double factorial2(int a)
{
    return 0;
}

int calcService::factorial(double a, double &result)
{
  result = factorial2(a);
  return SOAP_OK;
}

